"""
Ввод данных с клавиатуры:
- Имя
- Фамилия
- Возраст

Вывод данных в формате:
"Вас зовут <Имя> <Фамилия>! Ваш возраст равен <Возраст>."
"""

first_name = input("first name: ")
last_name = input("last name: ")
age = input("age: ")

print("Вас зовут ", first_name, ",", last_name, "! Ваш возраст равен ", age, ".", sep="")

# альтернативный способ f-string
print(f"Вас зовут {first_name} {last_name}! Ваш возраст равен {age}.")
