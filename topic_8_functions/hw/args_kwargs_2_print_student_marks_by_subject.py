def print_student_marks_by_subject(student_name, **marks):
    """
    Функция print_student_marks_by_subject.

    Принимает 2 аргумента: имя студента и именованные аргументы с оценками по различным предметам (**kwargs).

    Выводит на экран имя студента, а затем предмет и оценку (может быть несколько, если курс состоял из нескольких частей).

    Пример вызова функции print_student_marks_by_subject("Вася", math=5, biology=(3, 4), magic=(4, 5, 5)).
    """
    print(f"{student_name}")
    for x, y in marks.items():
        print(f"{x}: {y}")


print_student_marks_by_subject("Bob", math=(3, 2), astronomy=5)
