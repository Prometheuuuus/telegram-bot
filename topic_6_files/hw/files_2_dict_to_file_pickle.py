import pickle


def save_dict_to_file_pickle(my_file, my_dict):
    """
    Функция save_dict_to_file_pickle.

    Принимает 2 аргумента: строка (название файла или полный путь к файлу), словарь (для сохранения).

    Сохраняет список в файл. Загрузить словарь и проверить его на корректность.
    """
    with open(my_file, 'wb') as f:
        pickle.dump(my_dict, f)


if __name__ == '__main__':
    save_dict_to_file_pickle('task2.txt',
                             "Push it to the limit")
