def read_str_from_file(my_file):
    """
    Функция read_str_from_file.

    Принимает 1 аргумент: строка (название файла или полный путь к файлу).

    Выводит содержимое файла (в консоль). (* Файл должен быть предварительно создан и наполнен каким-то текстом.)
    """
    with open(my_file, "w") as file:
        file.write("Предварительно созданный файл, наполненный каким-то текстом")

    with open(my_file, "r") as file:
        print(file.read())


if __name__ == '__main__':
    my_file = 'task3.txt'
    read_str_from_file(my_file)