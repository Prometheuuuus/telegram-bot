def save_dict_to_file_classic(my_file, my_dict):
    """
    Функция save_dict_to_file_classic.

    Принимает 2 аргумента: строка (название файла или полный путь к файлу), словарь (для сохранения).

    Сохраняет список в файл. Проверить, что записалось в файл.
    """
    with open(my_file, 'w+') as file:
        file.write(str(my_dict))


if __name__ == '__main__':
    file_path = 'task1.txt'
    save_dict_to_file_classic(file_path,
                              ["Sometimes we laugh, sometimes we cry"])

    with open(file_path, 'r') as f:
        print(f.read())
