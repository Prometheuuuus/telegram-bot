import random

from final_project.player import Player
from final_project.nba_player_type import NbaPlayerType
from final_project.shot_range import ShotRange
from final_project.players_by_type import players_by_type


class PlayerNPC(Player):
    def __init__(self):
        rand_type_value = random.randint(NbaPlayerType.min_value(), NbaPlayerType.max_value())
        rand_player_type = NbaPlayerType(rand_type_value)

        rand_player_name = random.choice(list(players_by_type.get(rand_player_type, {}).keys()))

        super().__init__(rand_player_name, rand_player_type)

    def place(self):

        new_range = ShotRange(random.randint(ShotRange.min_value(), ShotRange.max_value()))

        super().place(new_range)


if __name__ == '__main__':
    player_npc = PlayerNPC()
    player_npc.place()
    print(player_npc)
