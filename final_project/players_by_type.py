from final_project.nba_player_type import NbaPlayerType
from final_project.shot_range import ShotRange

players_by_type = {
    NbaPlayerType.GUARD: {'Damian Lillard': ('Dame.png', {ShotRange.LONG: 0.38, ShotRange.MID: 0.52,
                                                          ShotRange.SHORT: 0.7}),
                          'James Harden': ('Beard.png', {ShotRange.LONG: 0.39, ShotRange.MID: 0.54,
                                                         ShotRange.SHORT: 0.7}),
                          'Stephen Curry': ('Curry.png', {ShotRange.LONG: 0.42, ShotRange.MID: 0.57,
                                                          ShotRange.SHORT: 0.7}),
                          'Zach LaVine': ('ZL.png', {ShotRange.LONG: 0.46, ShotRange.MID: 0.58,
                                                     ShotRange.SHORT: 0.8})},

    NbaPlayerType.FORWARD: {
        'LeBron James': ('LBJ.png', {ShotRange.LONG: 0.36, ShotRange.MID: 0.59,
                                     ShotRange.SHORT: 0.8}),
        'Kevin Durant': ('KD.png', {ShotRange.LONG: 0.4, ShotRange.MID: 0.58,
                                    ShotRange.SHORT: 0.8}),
        'Anthony Davis': ('AD.png', {ShotRange.LONG: 0.29, ShotRange.MID: 0.57,
                                     ShotRange.SHORT: 0.8}),
        'Giannis Antetokounmpo': ('Giannis.png', {ShotRange.LONG: 0.29, ShotRange.MID: 0.64,
                                                  ShotRange.SHORT: 0.85})},

    NbaPlayerType.CENTER: {
        "Shaquille O'Neal": ('Diesel.png', {ShotRange.LONG: 0, ShotRange.MID: 0.3,
                                            ShotRange.SHORT: 1}),
        'Pau Gasol': ('PG.png', {ShotRange.LONG: 0.35, ShotRange.MID: 0.48,
                                 ShotRange.SHORT: 0.9}),
        'Joel Embiid': ('Process.png', {ShotRange.LONG: 0.42, ShotRange.MID: 0.54,
                                        ShotRange.SHORT: 0.9}),
        'Nikola Jokic': ('Joker.png', {ShotRange.LONG: 0.42, ShotRange.MID: 0.59,
                                       ShotRange.SHORT: 0.9}), }
}
