from final_project.nba_player_type import NbaPlayerType
from final_project.shot_range import ShotRange
from random import choices
from final_project.players_by_type import players_by_type
from final_project.nba_blacktop import NbaBlackTop


class Player:
    game_point = 21

    def __init__(self,
                 name: str,
                 nba_player_type: NbaPlayerType):
        self.name = name
        self.points = 0
        self.nba_player_type = nba_player_type
        self._img, self._rng = players_by_type[nba_player_type][name]
        self.shot_range = None
        self.state = NbaBlackTop.READY

        # variants = [True, False]  # удачный удар / промах
        # self.probability = [rng[ShotRange.SHORT], rng[ShotRange.MID], rng[ShotRange.LONG]]  # вероятность удачного удара 80% и вероятность промаха 20%
        # self.distance = None
        # result = choices(self.point, self.probability)

    def throw(self, enemy_shot_range):
        if self.shot_range == enemy_shot_range:
            return "BLOCK "
        prob_score = self._rng[self.shot_range]
        prob_miss = 1 - prob_score
        result = choices([True, False], [prob_score, prob_miss])
        if result[0]:
            if self.shot_range == ShotRange.LONG:
                self.points += 2
                if self.points >= self.__class__.game_point:
                    self.state = NbaBlackTop.WINNER
                return "From downtown"
            else:
                self.points += 1
                if self.points >= self.__class__.game_point:
                    self.state = NbaBlackTop.WINNER
                return "Easy buckets"
        else:
            return "Miss"

    def place(self,
              new_range: ShotRange):
        self.shot_range = new_range

    def __str__(self):
        return f"Name: {self.name} | Type: {self.nba_player_type.name}\nPoints: {self.points}"


if __name__ == '__main__':
    p1 = Player("Damian Lillard", NbaPlayerType.GUARD)
    p2 = Player("Stephen Curry", NbaPlayerType.GUARD)
    p1.shot_range = ShotRange.LONG
    p2.shot_range = ShotRange.SHORT
    # throw(p2.shot_range)

    print(p1.throw(p2.shot_range))
    print(p1)
