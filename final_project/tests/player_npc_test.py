import random

from final_project.player_npc import PlayerNPC
from final_project.nba_blacktop import NbaBlackTop
from final_project.nba_player_type import NbaPlayerType


class TestPlayerNPCClass:
    player_name = "Anthony Davis"
    nba_player_type = NbaPlayerType.FORWARD
    points = 0

    def setup_method(self, method):
        random.seed(123)

    def test_init(self):
        player_test = PlayerNPC()

        assert player_test.name == self.__class__.player_name
        assert player_test.nba_player_type == self.__class__.nba_player_type
        assert player_test.points == self.__class__.points
        assert player_test.shot_range is None
        assert player_test.state == NbaBlackTop.READY

    def test_str(self):
        player_test = PlayerNPC()
        assert str(player_test) == f"Name: {self.__class__.player_name} | " \
                                   f"Type: {self.__class__.nba_player_type.name}\n" \
                                   f"Points: {self.__class__.points}"
