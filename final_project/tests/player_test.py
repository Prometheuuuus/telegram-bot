from final_project.player import Player
from final_project.nba_blacktop import NbaBlackTop
from final_project.nba_player_type import NbaPlayerType


class TestPlayerClass:
    player_name = "Damian Lillard"
    nba_player_type = NbaPlayerType.GUARD
    points = 0

    def test_init(self):
        player_test = Player(name=self.__class__.player_name,
                             nba_player_type=self.__class__.nba_player_type)

        assert player_test.name == self.__class__.player_name
        assert player_test.nba_player_type == self.__class__.nba_player_type
        assert player_test.points == self.__class__.points
        assert player_test.shot_range is None
        assert player_test.state == NbaBlackTop.READY

    def test_str(self):
        player_test = Player(name=self.__class__.player_name,
                             nba_player_type=self.__class__.nba_player_type)
        assert str(player_test) == f"Name: {self.__class__.player_name} | " \
                                   f"Type: {self.__class__.nba_player_type.name}\n" \
                                   f"Points: {self.__class__.points}"
