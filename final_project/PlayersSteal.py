from final_project.ShotRange import ShotRange
from final_project.nba_player_type import NbaPlayerType


players_steal = {
    NbaPlayerType.GUARD: {ShotRange.LONG: 0.38, ShotRange.MID: 0.52, ShotRange.SHORT: 0.7},
    NbaPlayerType.CENTER: {ShotRange.LONG: 0.38, ShotRange.MID: 0.52, ShotRange.SHORT: 0.7},
    NbaPlayerType.FORWARD: {ShotRange.LONG: 0.38, ShotRange.MID: 0.52, ShotRange.SHORT: 0.7},


}