from enum import Enum

GameResult = Enum("GameResult", "W L E")
