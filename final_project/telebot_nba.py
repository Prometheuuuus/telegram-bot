import json

import telebot
from telebot import types
from telebot.types import Message

from final_project.game_result import GameResult
from final_project.nba_blacktop import NbaBlackTop
from final_project.nba_player_type import NbaPlayerType
from final_project.player import Player
from final_project.player_npc import PlayerNPC
from final_project.players_by_type import players_by_type
from final_project.shot_range import ShotRange

with open("./bot_token.txt", 'r') as token_file:
    token = token_file.read().strip()

bot = telebot.TeleBot(token)

shot_range_keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True,
                                                one_time_keyboard=True,
                                                row_width=len(ShotRange))
statistics = {}
stat_file = "game_stat.json"

shot_range_keyboard.row(*[types.KeyboardButton(range_part.name) for range_part in ShotRange])

# {id: {'user_player': Player_obj, 'npc_player': PlayerNPC_obj}}
state = {}


@bot.message_handler(commands=["help", "info"])
def help_command(message):
    bot.send_message(message.chat.id, "Hi")


@bot.message_handler(commands=["stat"])
def stat(message):
    global statistics
    if statistics.get(str(message.chat.id), None) is None:
        user_stat = "Еще не было ни одной игры"
    else:
        user_stat = "Твои результаты:"
        for res, num in statistics[str(message.chat.id)].items():
            user_stat += f"\n{res}: {num}"

    bot.send_message(message.chat.id,
                     text=user_stat)


@bot.message_handler(commands=["start"])
def start(message):
    yes_no_keyboard = types.ReplyKeyboardMarkup(resize_keyboard=True,
                                                one_time_keyboard=True,
                                                row_width=2)
    yes_no_keyboard.row("Да", "Нет")

    bot.send_message(message.from_user.id,
                     text="Ты готов ?",
                     reply_markup=yes_no_keyboard)

    bot.register_next_step_handler(message, start_question_handler)


def update_save_stat(user_id, result: GameResult):
    print("Обновление статистики", end="...")
    global statistics

    if statistics.get(user_id, None) is None:
        statistics[user_id] = {}

    if result == GameResult.W:
        statistics[user_id]['W'] = statistics[user_id].get('W', 0) + 1
    elif result == GameResult.L:
        statistics[user_id]['L'] = statistics[user_id].get('L', 0) + 1
    else:
        print(f"Не существует результата {result}")

    with open(stat_file, 'w') as file:
        json.dump(statistics, file)

    print("завершено!")


def load_stat():
    print('Загрузка статистики...', end='')
    global statistics

    try:
        with open(stat_file, 'r') as file:
            statistics = json.load(file)
        print('завершена успешно!')
    except FileNotFoundError as my_error:
        statistics = {}
        print('файл не найден!')


def start_question_handler(message):
    if message.text.lower() == 'да':
        bot.send_message(message.from_user.id,
                         'Хорошо, начинаем!')

        create_npc(message)

        ask_user_about_player(message)

    elif message.text.lower() == 'нет':
        bot.send_message(message.from_user.id,
                         'Ок, я подожду.')
    else:
        bot.send_message(message.from_user.id,
                         'Я не знаю такого варианта ответа!')


def create_npc(message):
    print(f"Начало создания объекта NPC для chat id = {message.chat.id}")
    global state
    player_npc = PlayerNPC()
    if state.get(message.chat.id, None) is None:
        state[message.chat.id] = {}
    state[message.chat.id]['npc_player'] = player_npc

    npc_image_filename = players_by_type[player_npc.nba_player_type][player_npc.name][0]
    bot.send_message(message.chat.id, 'Соперник:')
    with open(f"./images/{npc_image_filename}", 'rb') as file:
        bot.send_photo(message.chat.id, file, player_npc)
    print(f"Завершено создание объекта NPC для chat id = {message.chat.id}")


def ask_user_about_player(message):
    markup = types.InlineKeyboardMarkup()

    for nba_player_type in NbaPlayerType:
        markup.add(types.InlineKeyboardButton(text=nba_player_type.name,
                                              callback_data=f"nba_player_type_{nba_player_type.value}"))

    bot.send_message(message.chat.id, "Выбери тип игрока:", reply_markup=markup)


@bot.callback_query_handler(func=lambda call: "player_type_" in call.data)
def player_type_handler(call):
    call_data_split = call.data.split("_")
    if len(call_data_split) < 4 or not call_data_split[3].isdigit():
        bot.send_message(call.message.chat.id, "Возникла проблема. Перезапусти сессию!")
    else:
        player_type_id = int(call_data_split[3])

        bot.send_message(call.message.chat.id, "Выбери своего игрока:")

        ask_user_about_player_by_type(player_type_id, call.message)


def ask_user_about_player_by_type(player_type_id, message):
    player_type = NbaPlayerType(player_type_id)
    player_dict_by_type = players_by_type.get(player_type, {})

    for player_name, player_img in player_dict_by_type.items():
        markup = types.InlineKeyboardMarkup()
        markup.add(types.InlineKeyboardButton(text=player_name,
                                              callback_data=f"player_name_{player_type_id}_{player_name}"))
        with open(f"./images/{player_img[0]}", 'rb') as file:
            bot.send_photo(message.chat.id, file, reply_markup=markup)


@bot.callback_query_handler(func=lambda call: "player_name_" in call.data)
def player_name_handler(call):
    call_data_split = call.data.split("_")
    if len(call_data_split) < 4 or not call_data_split[2].isdigit():
        bot.send_message(call.message.chat.id, "Возникла проблема. Перезапусти сессию!")
    else:
        player_type_id, player_name = int(call_data_split[2]), call_data_split[3]

        create_user_player(call.message, player_type_id, player_name)

        bot.send_message(call.message.chat.id, "Игра началась!")

        game_next_step(call.message)


def create_user_player(message, player_type_id, player_name):
    print(f"Начало создания объекта Player для chat id = {message.chat.id}")
    global state
    user_player = Player(name=player_name,
                         nba_player_type=NbaPlayerType(player_type_id))

    if state.get(message.chat.id, None) is None:
        state[message.chat.id] = {}
    state[message.chat.id]['user_player'] = user_player

    image_filename = players_by_type[user_player.nba_player_type][user_player.name]
    bot.send_message(message.chat.id, 'Твой выбор:')
    with open(f"./images/{image_filename[0]}", 'rb') as file:
        bot.send_photo(message.chat.id, file, user_player)

    print(f"Завершено создание объекта Player для chat id = {message.chat.id}")


def game_next_step(message: Message):
    bot.send_message(message.chat.id,
                     "Место броска:",
                     reply_markup=shot_range_keyboard)

    bot.register_next_step_handler(message, reply_attack)


def reply_attack(message: Message):
    if not ShotRange.has_item(message.text):
        bot.send_message(message.chat.id, "Необходимо выбрать вариант на клавиатуре!")
        game_next_step(message)
    else:
        attack_body_part = message.text

        global state
        # обращение по ключу ко вложенному словарю для получения объекта покемона пользователя
        user_player = state[message.chat.id]['user_player']

        # обращение по ключу ко вложенному словарю для получения объекта покемона npc
        player_npc = state[message.chat.id]['npc_player']

        # конвертировать строку хранящуюся в объекте attack_body_part в правильный тип enum BodyPart
        # конвертировать строку хранящуюся в объекте defend_body_part в правильный тип enum BodyPart
        user_player.place(new_range=ShotRange[attack_body_part])

        player_npc.place()

        game_step(message, user_player, player_npc)


def game_step(message: Message, user_player: Player, player_npc: Player):
    comment_npc = player_npc.throw(enemy_shot_range=user_player.shot_range)
    bot.send_message(message.chat.id, f"NPC player: {comment_npc}\nPoints: {player_npc.points}")

    comment_user = user_player.throw(enemy_shot_range=player_npc.shot_range)
    bot.send_message(message.chat.id, f"Your player: {comment_user}\nPoints: {user_player.points}")

    if player_npc.state == NbaBlackTop.READY and user_player.state == NbaBlackTop.READY:
        bot.send_message(message.chat.id, "Продолжаем!")
        game_next_step(message)
    elif player_npc.state == NbaBlackTop.WINNER:
        sti1 = open(f"./stickers/sticker.webp", "rb")
        bot.send_sticker(message.chat.id, sti1)
        bot.send_message(message.chat.id, "Ты проиграл")
        update_save_stat(message.chat.id, GameResult.L)
    elif user_player.state == NbaBlackTop.WINNER:
        sti = open(f"./stickers/sticker1.webp", "rb")
        bot.send_sticker(message.chat.id, sti)
        bot.send_message(message.chat.id, "Ты победил")
        update_save_stat(message.chat.id, GameResult.W)


if __name__ == '__main__':
    load_stat()

    print('Starting the bot...')
    bot.polling(none_stop=True, interval=0)
    print('The bot has stopped!')
