from enum import Enum, auto


class NbaPlayerType(Enum):
    FORWARD = auto()
    GUARD = auto()
    CENTER = auto()

    @classmethod
    def min_value(cls):
        return cls.FORWARD.value

    @classmethod
    def max_value(cls):
        return cls.CENTER.value
