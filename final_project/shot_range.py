from enum import Enum, auto


class ShotRange(Enum):
    LONG = auto()
    MID = auto()
    SHORT = auto()

    @classmethod
    def min_value(cls):
        return cls.LONG.value

    @classmethod
    def max_value(cls):
        return cls.SHORT.value

    @classmethod
    def has_item(cls, name: str):
        return name in cls._member_names_

