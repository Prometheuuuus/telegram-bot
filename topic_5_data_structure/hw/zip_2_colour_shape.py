def zip_colour_shape(color, shape):
    """
    Функция zip_colour_shape.

    Принимает 2 аргумента: список с цветами(зеленый, красный) и кортеж с формами (квадрат, круг).

    Возвращает список с парами значений из каждого аргумента.

    Если вместо list передано что-то другое, то возвращать строку 'First arg must be list!'.
    Если вместо tuple передано что-то другое, то возвращать строку 'Second arg must be tuple!'.

    Если list пуст, то возвращать строку 'Empty list!'.
    Если tuple пуст, то возвращать строку 'Empty tuple!'.

    Если list и tuple различного размера, обрезаем до минимального (стандартный zip).
    """
    if type(color) != list:
        return 'First arg must be list!'
    elif type(shape) != tuple:
        return 'Second arg must be tuple!'
    if len(color) == 0:
        return 'Empty list!'
    elif len(shape) == 0:
        return 'Empty tuple!'
    return list(zip(color, shape))
