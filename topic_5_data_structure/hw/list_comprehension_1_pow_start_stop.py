def pow_start_stop(start, stop):
    """
    Функция pow_start_stop.

    Принимает числа start, stop.

    Возвращает список состоящий из квадратов значений от start до stop (не включая).

    Пример: start=3, stop=6, результат [9, 16, 25].

    Если start или stop не являются int, то вернуть строку 'Start and Stop must be int!'.
    """
    if not (type(start) == type(stop) == int):
        return 'Start and Stop must be int!'

    return [i ** 2 for i in range(start, stop)]