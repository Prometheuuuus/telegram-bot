def dict_to_list(my_dict):
    """
    Функция dict_to_list.

    Принимает 1 аргумент: словарь.

    Возвращает список: [
        список ключей,
        список значений,
        количество уникальных элементов в списке ключей,
        количество уникальных элементов в списке значений
    ].

    Если вместо словаря передано что-то другое, то возвращать строку 'Must be dict!'.

    Если dict пуст, то возвращать ([], [], 0, 0).
    """
    if type(my_dict) != dict:
        return 'Must be dict!'
    else:
        return list(my_dict.keys()), list(my_dict.values()), len(set(my_dict.keys())), len(set(my_dict.values()))
