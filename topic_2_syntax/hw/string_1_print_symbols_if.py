def print_symbols_if(my_str):
    """
    Функция print_symbols_if.
    
    Принимает строку.
    
    Если строка нулевой длины, то вывести строку "Empty string!".
    
    Если длина строки больше 5, то вывести первые три символа и последние три символа.
    Пример: string='123456789' => result='123789'
    
    Иначе вывести первый символ столько раз, какова длина строки.
    Пример: string='345' => result='333'
    """
    if my_str == "":
        print("Empty string!")
    elif len(my_str) > 5:
        print(my_str[:3] + my_str[-3:])
    else:
        print(len(my_str) * my_str[0])
