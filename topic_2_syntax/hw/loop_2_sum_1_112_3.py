def sum_1_112_3():
    """
    Функция sum_1_112_3.

    Вернуть сумму 1+4+7+10+...109+112.
    """
    acc = 0
    for i in range(1,113,3):
        acc += i

    return acc
