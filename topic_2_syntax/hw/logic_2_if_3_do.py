def if_3_do(figure):
    """
    Функция if_3_do.

    Принимает число.
    Если оно больше 3, то увеличить число на 10, иначе уменьшить на 10.
    Вернуть результат.
    """

    if figure > 3:
        return figure + 10
    else:
        return figure - 10



