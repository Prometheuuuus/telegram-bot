def check_substr(my_str, my_str2):
    """
    Функция check_substr.

    Принимает две строки.
    Если меньшая по длине строка содержится в большей, то возвращает True,
    иначе False.
    Если строки равны, то False.
    Если одна из строк пустая, то True.
    """
    if my_str == my_str2:
        return False
    elif my_str == "" or my_str2 == "":
        return True
    elif len(my_str) < len(my_str2):
        return True if my_str in my_str2 else False
    elif len(my_str2) < len(my_str):
        return True if my_str2 in my_str else False


